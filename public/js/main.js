import Vue from 'vue'
import App from './App'
import FishUI from 'fish-ui'

Vue.use(FishUI)

$(document).ready(function () {
  // Mobile-menu
  $('.header__hamburger-toggle').on('click', function () {
    $('.header__menu-slide').addClass('header__menu-slide--active');
    $('.header__nav').addClass('header__nav--active');
  });
  $('.menu-slide__btn-close').on('click', function () {
    $('.header__menu-slide').removeClass('header__menu-slide--active');
    $('.header__nav').removeClass('header__nav--active');
  });

  // Smooth scroll
  $('.header__menu-list li a, .menu-slide__item a, #header-registration-btn').on('click', function () {
    var el = $(this);
    var dest = el.attr('href');
    if (dest !== undefined && dest !== '') {
      $('html, body').animate({
        scrollTop: $(dest).offset().top
      }, 700);
    }
    return false;
  });


  /**
   * Checkboxes
   */
  $('.need-tshirt-label').click( function() {
    var checkbox = $(this).data('checkbox');
    var list = $(this).data('list');

    // Если есть checkbox
    if ($('#'+checkbox).length) {
      // Прячем список размеров
      $('#'+list).toggle();

      // Обнуляем значение и деактивируем все размеры в данном list-элементе
      if ($('#'+checkbox).prop('checked') == 1) {
        $('#'+list+' .tshirt_size_control').val(0);
        $('#'+list+' .size-item').removeClass('active');
      }

      // Переключаем чекбокс
      $('#'+checkbox).prop('checked', !$('#'+checkbox).prop('checked'));
    }
  });

  $('.need-tshirt-label').each(function( index, item ) {
    var checkbox = $(item).data('checkbox');
    var list = $(item).data('list');
    if ($('#'+checkbox).length) {
      if ($('#'+checkbox).prop('checked') != 1) {
        $('#'+list).toggle();
        $('#'+list+' .tshirt_size_control').val(0);
        $('#'+list+' .size-item').removeClass('active');
      }
    }
  });


  $('.size-item').click( function () {
    var list_id = $(this).data('list');
    // Деактивируем все размеры в этом листе
    $('#'+list_id+' .size-item').removeClass('active');

    // Активируем размер
    $(this).addClass('active');

    // Берем значение и parent-control
    var size    = $(this).data('size');
    var control = $(this).data('control');

    // Устанавливаем значение parent-control
    $('#'+control).val(size);
  });


  /**
   * Video player
   */
  $('#show-video').click(function () {
    $('#video-modal-fader').addClass('video-modal-fader-visible');
  });
  $('#video-modal-fader').click(function () {
    var iframe = $('#vimeo-player iframe')[0];
    var player = $f(iframe);
    player.api('pause');
    $('#video-modal-fader').removeClass('video-modal-fader-visible');
  });


  /**
   * Form togglers
   */
  $('.form').hide();
  $('.show-form-trigger').on('click', function() {
    var form_id = $(this).data('form-id');
    if ($(this).hasClass('form-slide-active')) {
      $('.form').slideUp();
      $(this).removeClass('form-slide-active');
    }
    else {
      $('.form').slideUp();
      $('.show-form-trigger').removeClass('form-slide-active');
      $(this).addClass('form-slide-active');
      $('#form-'+form_id).slideToggle();
    }

  });

  /**
   * Wow animation
   */
  new WOW().init();
});
