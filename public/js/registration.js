/**
 * Toastr config
 */
var $toastr_config = {
	timeOut: 7000,
	progressBar: true,
	positionClass: "toast-top-center"
};


$(document).ready(function () {
	/**
	 * Signin
	 */
	$('.signin-link').click( function() {
		$('#signin-section').removeClass('hidden');
	});

	$('#signin').click( function(event) {
		event.preventDefault();
		var btn = $(this);
		btn.prop('disabled', true).addClass('btn-loading');

		// Get signin data
		var data = {};
		$('#signin-form input').each( function(i, item) {
			data[$(item).attr('name')] = $(item).val();
		});
		data['_token'] = $('meta[name="csrf-token"]').attr('content');


		$.post( '/api/auth/send-auth-link', data, function (r) {
			if(typeof r.result !== 'undefined') {
				if(r.result === 'error') {
					$('#signin-form #error-block').removeClass('hidden');
				}
				else {
					$('#signin-form #error-block').removeClass('hidden').removeClass('alert-warning').addClass('alert-success');
					$('#signin-form #error-block').html(r.message);
					// btn.remove();
				}
			}
		})
		.done(function () {
		})
		.fail(function (error) {
			console.log(error);
		})
		.always(function() {
			btn.prop('disabled', false).removeClass('btn-loading');
		});
	});

	/**
	 * Signup
	 */
	$('.signup').click( function(event) {
		event.preventDefault();
		var btn = $(this);
    // btn.prop('disabled', true).addClass('btn-loading');
    var form_id = btn.data('form');

		// Get ticket data
		var ticket = {};
		$('#'+form_id+' input').each( function(i, item) {
      if ($(item).attr('type') === 'checkbox') {
				ticket[$(item).attr('name')] = ($(item).is(':checked')) ? 1 : 0;
			}
			else {
				ticket[$(item).attr('name')] = $(item).val();
			}
		});

		var data = {
			"ticket": ticket,
			"_token": $('meta[name="csrf-token"]').attr('content'),
		};

    // return true;

		$.post( '/api/auth/signup', data, function (r) {
			console.log(r);
			$('.error-display').remove();
			$('.validate-block').removeClass('has-error');

			if (typeof r.result !== 'undefined') {
				if (r.result === 'error') {
          $('#'+form_id+' .error-block').removeClass('hidden');
          var msg_html = '<div>'+r.message+'</div>';
          msg_html += '<ul>';
          $.each(r.errors, function( index, item ) {
            msg_html += '<li>' +item+ '</li>';
          });
          msg_html += '</ul>';
          $('#'+form_id+' .error-block').html(msg_html);


					// $.each(r.errors, function(i, item) {
						// Validate messages
						// var validate_block = $('#'+i).closest('.validate-block').first();
						// if (validate_block.length > 0 && i.length > 1) {
						// 	var text = '';
						// 	$.each(item, function (key, val) {
						// 		text += val;
						// 	});
						// 	var el = '<i class="error-display" data-placement="left" data-toggle="tooltip" title="'+text+'">!</i>';
						// 	$(validate_block).addClass('has-error').append(el);
						// }

            // Validate messages for teamates
            /*
						if (i.toString().length === 1) {
							j = parseInt(i);
							$.each(item, function (key, teamate) {
								var text = '';
								$.each(teamate, function (k, val) {
									text += val;
								});
								var el = '<i class="error-display" data-placement="left" data-toggle="tooltip" title="'+text+'">!</i>';
								$('#'+key+j).closest('.validate-block').first().addClass('has-error').append(el);
							});
						}*/
					// });
				}
				else {
					$('#'+form_id+' .error-block').removeClass('hidden').removeClass('alert-danger').addClass('alert-success');
					$('#'+form_id+' .error-block').html(r.message);
					btn.remove();
				}
		}
		})
		.done(function () {
			$('[data-toggle="tooltip"]').tooltip();
		})
		.fail(function (error) {
			console.log(error);
		})
		.always(function() {
			btn.prop('disabled', false).removeClass('btn-loading');
		});
	});
});




/**
 * Check errors
 */
function errorsHas(index, errors) {
	let r = false;
	for(var key in errors) {r = (index === key) ? true : r; }
	return r;
}

/**
 * Get error message
 */
function errorsGetMessage(index, errors) {
	let m = '';
	for(var key in errors) {
		if(index === key) { errors[key].forEach(function(item) { m = m + '<div>' + item + '<div>'; }); }
		else { m = m; }
	}
	return m;
}
