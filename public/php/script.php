<?php

function connect_to_mysql(){
  $servername = "localhost";
  $username = "localuser";
  $password = "12345678";
  $dbname = "music_mysql";
  return new mysqli($servername, $username, $password, $dbname);
}

function connect_to_pgsql(){
  return pg_connect("
    host=localhost
    dbname=music_pgsql
    user=localuser
    password=12345678
  ");
}

function connect_to_oracle(){
  $username = "localuser";
  $password = "12345678";
  $db = "oci:dbname=orcl";
  return new PDO($db, $username, $password);
}

function connect_to_sqlsrv(){
  $serverName = "DESKTOP-8D7QI6T";
  $connectionInfo = [
    "Database" => "music_mssql",
  ];
  return sqlsrv_connect($serverName, $connectionInfo);
}

function get_query($searchstr, $where) {
  $sql =
  "SELECT album.title a_title, author.name a_name, song.title s_title
  FROM song
  JOIN album ON album.id = song.album_id
  JOIN author_song ON song.id = author_song.song_id
  JOIN author ON author.id = author_song.author_id";

  // build where
  $where_sql = " WHERE ( ";
  $where_sql .= "song.title LIKE " . "'".$searchstr."'";

  if ($where['album'] == 'true')
    $where_sql .= " OR " . "album.title LIKE " . "'".$searchstr."'";

  if ($where['author'] == 'true')
    $where_sql .= " OR " . "author.name LIKE " . "'".$searchstr."'";

  $where_sql .= ");";
  // \build where

  $sql .= $where_sql;

  $arr = [];

  if ($_GET['dbname'] == "mysql") {
    $result = connect_to_mysql()->query($sql);

    $i = "1";
    while($row = $result->fetch_assoc()) {
      $arr[$i-1] = $row;
      $i++;
    }
  }

  if ($_GET['dbname'] == "pgsql") {
    $result = pg_query(connect_to_pgsql(), $sql);

    $i = "1";
    while ($row = pg_fetch_array($result)) {
      $arr[$i-1] = $row;
      $i++;
    }
  }

  if ($_GET['dbname'] == "sqlsrv") {
    // $sql = "SELECT * FROM song";
    // $stmt = sqlsrv_query(connect_to_sqlsrv(), $sql);

    // $arr[0] = $stmt;

    $stmt = sqlsrv_query(connect_to_sqlsrv(), $sql);

    $i = "1";
    while ($row = sqlsrv_fetch_array($stmt)) {
      $arr[$i-1] = $row;
      $i++;
    }
    $arr = sqlsrv_errors();
  }

  if ($_GET['dbname'] == "oracle") {
    // $conn = oci_connect('localuser', '12345678', 'orcl');

    $pdo = connect_to_oracle();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // $sqll = "select * from song";

    // $arr = $pdo->query($sql);

    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // $arr = $result;
    // $i = "1";
    // while ($row = $stmt->fetchAll()) {
    //   $arr[$i-1] = $row;
    //   $i++;
    // }
  }

  return $arr;
}


if (isset($_GET['tag'])) {
  $marks = [
    'album' => $_GET['album'],
    'author' => $_GET['author'],
  ];
  $arr = get_query('%'.$_GET['input'].'%', $marks);
  $arr = json_encode($arr);
  echo $arr;
}

?>
