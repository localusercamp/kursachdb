<html>

<head>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.base.css" rel="stylesheet">
  <link href="css/style.back.css" rel="stylesheet">
  <link href="css/datepicker.min.css" rel="stylesheet">

  <script src="/js/vendors/axios.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
  <script src="/js/vendors/jquery-3.4.1.min.js"></script>
  <script src="/js/vendors/jquery.mask.min.js"></script>
  <script src="/js/vendors/datepicker/datepicker.min.js"></script>
  <script src="/js/vendors/datepicker/i18n/datepicker.ru-RU.js"></script>
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
  <link rel="stylesheet" href="https://cdn.bootcss.com/font-awesome/4.7.0/css/font-awesome.css"/>
  <link rel="stylesheet" href="https://fonts.proxy.ustclug.org/css?family=Lato:400,700,400italic,700italic&subset=latin"/>
</head>

<body>
<div class="app" id="app">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12" style="padding: 0 0">

        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">

              <div class="panel-heading">

                <div class="col-md-3">
                  <div class="form-group">
                    <input type="text" class="form-control" v-model="input" placeholder="Что ищем?">
                  </div>

                  <div class="col-md-3">
                    Альбом
                    <input v-model="album" type="checkbox">
                  </div>

                  <div class="col-md-4">
                    Исполнитель
                    <input v-model="author" type="checkbox">
                  </div>

                  <div class="col-md-5">
                    <select v-model="dbname">
                      <option value="mysql">MySQL</option>
                      <option value="pgsql">PostgresSQL</option>
                      <option value="oracle">Oracle</option>
                      <option value="sqlsrv">MsSQL</option>
                    </select>
                  </div>

                </div>

                <div class="col-md-1">
                  <button class="btn btn-primary" id="button"  v-on:click="select()">
                    Показать
                  </button>
                </div>

                <div class="col-md-8">
                  <div class="col-md-3">
                    <input type="text" class="form-control" v-model="create.title" placeholder="Название песни">
                  </div>
                  <div class="col-md-3">
                    <input type="text" class="form-control" v-model="create.text" placeholder="Текст песни">
                  </div>
                  <div class="col-md-3">
                    <input type="number" min='1' max="100" class="form-control" v-model="create.album_id" placeholder="ID альбома">
                  </div>
                  <div class="col-md-3">
                    <button class="btn btn-primary" id="button"  v-on:click="insert()">
                      Создать
                    </button>
                  </div>
                </div>

              </div>

              <table v-if="show" id="table" class="table table-condensed table-bordered table-hover table-striped">

                <thead>
                  <tr>
                    <th>Песня</th>
                    <th>Исполнитель</th>
                    <th>Альбом</th>
                  </tr>
                </thead>

                <tbody>
                    <tr v-for="item in data">
                      <td>@{{ item.s_title }}</td>
                      <td>@{{ item.a_name }}</td>
                      <td>@{{ item.a_title }}</td>
                    </tr>
                </tbody>

              </table>

            </div>
          </div>
        </div>

        <div v-if="show_hint" class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="panel-heading">
                Ничего не найдено
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>


  <script>

    const app = new Vue({
      el: '#app',
      data: {
        show: false,
        show_hint: false,
        input: "",
        data: [],
        album: false,
        author: false,
        dbname: "mysql",
        create: {
          title: '',
          text: '',
          album_id: '',
        }
      },
      methods: {
        select(){
          let that = this;

          if (that.dbname == 'sqlsrv') {
            axios.post('/select',{
              db: that.dbname
            }).then((response) => {
              console.log({'data':response.data.select});
              that.data = response.data.select;

              if(Object.keys(response.data.select).length > 0) {
                that.show = true;
                that.show_hint = false;
              }
              else {
                that.show = false;
                that.show_hint = true;
              }
            })
          }
          else {
            axios.get(
              'php/script.php?tag=true'
              +'&input=' + that.input
              +'&album=' + that.album
              +'&author=' + that.author
              +'&dbname=' + that.dbname
            )
            .then(function(response){
              console.log({'data':response.data});
              that.data = response.data;

              if(Object.keys(response.data).length > 0) {
                that.show = true;
                that.show_hint = false;
              }
              else {
                that.show = false;
                that.show_hint = true;
              }
            });
          }

        },
        insert() {
          let that = this;
          if (that.dbname == 'oracle') {
            axios.get(
              'php/oracleinsert.php?tag=true'
              +'&title=' + that.create.title
              +'&text=' + that.create.text
              +'&album_id=' + that.create.album_id
            ).then((r) => {
              console.log({'data':r.data})
            })
          }
          else {
            axios.post('/insert', {
              fields: that.create,
              db: that.dbname
            }).then(() => {
              that.select();
            })
          }
        }
      },
    });
  </script>


</body>
</html>


