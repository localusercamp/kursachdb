<?php
$servername = "localhost";
$username = "localuser";
$password = "12345678";
$dbname = "music";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT album.title a_title, author.name a_name, song.title s_title
        FROM song
        JOIN album ON album.id = song.album_id
        JOIN author_song ON song.id = author_song.song_id
        JOIN author ON author.id = author_song.author_id";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "Альбом: ".      $row["a_title"]. " ".
             "Песня: ".       $row["s_title"]. " ".
             "Исполнитель: ". $row["a_name"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>
