<?php

function connect_to_mysql(){
  $servername = "localhost";
  $username = "localuser";
  $password = "12345678";
  $dbname = "music_mysql";
  return new mysqli($servername, $username, $password, $dbname);
}

function get_query($where){
  $sql = "SELECT album.title a_title,
                 author.name a_name,
                 song.title s_title
          FROM song
          JOIN album ON album.id = song.album_id
          JOIN author_song ON song.id = author_song.song_id
          JOIN author ON author.id = author_song.author_id
          WHERE song.title LIKE '". $where. "'";

  $result = connect_to_mysql()->query($sql);

  while($row = $result->fetch_assoc()) {
    yield $row;
  }
}

?>

<link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.base.css" rel="stylesheet">
  <link href="css/style.back.css" rel="stylesheet">
  <link href="css/datepicker.min.css" rel="stylesheet">

  <script src="/js/vendors/axios.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.15/lodash.min.js"></script>
  <script src="/js/vendors/jquery-3.4.1.min.js"></script>
  <script src="/js/vendors/jquery.mask.min.js"></script>
  <script src="/js/vendors/datepicker/datepicker.min.js"></script>
<script src="/js/vendors/datepicker/i18n/datepicker.ru-RU.js"></script>








{{--
select album.title, author.name, song.title
from song
join album on album.id = song.album_id
join author_song on song.id = author_song.song_id
join author on author.id = author_song.author_id
--}}

<form name="form" action="" method="get">
  <input type="text" name="subject" id="subject" value="Car Loan">
</form>


<button>
  Кнопка
</button>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12" style="padding: 0 0">

      <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">

            <div class="panel panel-default" v-if="user_list.length > 0">

              <div class="panel-heading">
                <span class="chunk bg-green">Песни</span><br>
              </div>

              <table class="table table-condensed table-bordered table-hover table-striped">

                <thead>
                  <tr>
                    <th>Песня</th>
                    <th>Исполнитель</th>
                    <th>Альбом</th>
                  </tr>
                </thead>

                <tbody>
                  @foreach (get_query('%s%') as $item)
                    <tr>
                      <td>{{$item['s_title']}}</td>
                      <td>{{$item['a_title']}}</td>
                      <td>{{$item['a_name']}}</td>
                    </tr>
                  @endforeach
                </tbody>

              </table>

            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
</div>












{{-- <div class="col-md-3">
              <div class="form-group">
                <label for="">Тариф</label>
                <select v-model="form.tariff_id" class="form-control" placeholder="Тариф">
                  <option v-for="tariff in form.tariff_list" :key="tariff.id" :value="tariff.id" >@{{tariff.distance}}</option>
                </select>
              </div>
            </div>

            <div class="col-md-3">
              <div class="form-group">
                <label for="">Email</label>
                <input type="text" v-model="form.email" class="form-control" placeholder="Email">
              </div>
            </div>

            <div class="col-md-2" style="padding-top: 24px;">
              <button class="btn btn-primary" @click="search()">Поиск</button>
              <a  v-if="form.selected_tariff_id === null" class="ml-2 btn btn-default" disabled title="Выбери тариф и нажми Поиск">Экспорт в Excel</a>
              <a v-else class="ml-2 btn btn-default" @click="search('xls')">Экспорт в Excel</a>
            </div>

            <div class="col-md-4 text-right" style="padding-top: 24px;">
              <a href="{{ config('app.url') }}/api/payment/recalculate?key=truebrave" class="btn btn-default">Переназначить номера</a>
              <a href="{{ config('app.url') }}/api/payment/check-payments?key=truebrave" class="btn btn-default">Протолкнуть платежи</a>
            </div> --}}
