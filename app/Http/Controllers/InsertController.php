<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InsertController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function insert(Request $request)
  {
    if ($request['db'] == 'oracle') {
      DB::connection($request['db'])->insert('insert into song (id, title, text, album_id, created_at) values (?, ?, ?, ?, ?)',
        [
          DB::connection($request['db'])->table('song')->max('id')+1,
          $request['fields']['title'],
          $request['fields']['text'],
          $request['fields']['album_id'],
          date('Y-m-d')
        ]
      );
    }
    else {
      DB::connection($request['db'])->insert('insert into song (title, text, album_id, created_at) values (?, ?, ?, ?)',
        [
          $request['fields']['title'],
          $request['fields']['text'],
          $request['fields']['album_id'],
          date('Y-m-d')
        ]
      );
    }
    DB::connection($request['db'])->insert('insert into author_song (author_id, song_id) values (?, ?)',
      [
        1,
        DB::connection($request['db'])->table('song')->max('id')
      ]
    );
    // DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
  }

}
