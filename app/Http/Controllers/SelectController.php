<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SelectController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function select(Request $request)
  {
    $select = DB::connection($request['db'])->select("
      SELECT album.title a_title, author.name a_name, song.title s_title
      FROM song
      JOIN album ON album.id = song.album_id
      JOIN author_song ON song.id = author_song.song_id
      JOIN author ON author.id = author_song.author_id
    ");
    return compact('select');
  }

}
