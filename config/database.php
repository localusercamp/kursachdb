<?php

return [
    // 'default' => 'mysql',
    'connections' => [
        'mysql' => [
          'driver' => 'mysql',
          'host' => env('DB_HOST'),
          'database' => env('DB_DATABASE'),
          'username' => env('DB_USERNAME'),
          'password' => env('DB_PASSWORD'),
        ],
        'pgsql' => [
          'driver' => 'pgsql',
          'database' => env('DB2_DATABASE'),
          'username' => env('DB_USERNAME'),
          'password' => env('DB_PASSWORD'),
        ],
        'sqlsrv' => [
          'driver' => 'sqlsrv',
          'host' => env('DB3_HOST'),
          'database' => env('DB3_DATABASE'),
          // 'username' => env('DB3_USERNAME'),
          // 'password' => env('DB3_PASSWORD'),
        ],
        'oracle' => [
          'driver' => 'oci',
          'host' => 'localhost',
          // 'port' => '1521',
          'database' => 'orcl',
          'username' => 'localuser',
          'password' => '12345678',
        ]
    ]
];
