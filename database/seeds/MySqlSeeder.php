<?php

use Illuminate\Database\Seeder;

class MySqlSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();

    DB::table('genre')->insert([
      [
        'title' => 'Rock'
      ],[
        'title' => 'Hip-Hop'
      ],[
        'title' => 'Country'
      ],[
        'title' => 'Jazz'
      ],[
        'title' => 'Electronic'
      ],
    ]);

    for($i = 0; $i < 30; $i++) {
      DB::table('subgenre')->insert([
        'title' => $this->make_title('subgenre'),
        'genre_id' => $faker->numberBetween(1,5),
      ]);
    }

    for($i = 0; $i < 40; $i++) {
      DB::table('concert')->insert([
        'title' => $faker->state . ' concert',
        'city' => $faker->city,
        'visitors_count' => $faker->randomNumber,
        'premiere_date' => $faker->date,
      ]);
    }

    for($i = 0; $i < 20; $i++) {
      DB::table('album')->insert([
        'title' => $this->make_title('album'),
        'created_at' => $faker->date,
      ]);
    }

    for($i = 0; $i < 100; $i++) {
      DB::table('song')->insert([
        'title' => $this->make_title('song'),
        'text' => $faker->text,
        'created_at' => $faker->date,
        'album_id' => $faker->numberBetween(1, 90),
      ]);
    }

    for($i = 0; $i < 30; $i++) {
      DB::table('author')->insert([
        'name' => $this->make_title('author'),
        'started_at' => $faker->date,
        'finished_at' => $faker->date,
      ]);
    }

    for($i = 0; $i < 10; $i++) {
      DB::table('musician')->insert([
        'firstname' => $faker->firstname,
        'surname' => $faker->lastname,
        'nickname' => $this->make_nick(),
        'author_id' => $faker->numberBetween(1, 30),
      ]);
    }

    for($i = 1; $i < 101; $i++) {
      DB::table('author_song')->insert([
        'author_id' => $faker->numberBetween(1,30),
        'song_id' => $i,
      ]);
    }

  }

  public function make_title($type)
  {
    $faker = Faker\Factory::create();

    $n = 0;

    if ($type == 'album')
      $n = rand(1,2);
    else if ($type == 'author')
      $n = rand(1,2);
    else if ($type == 'song')
      $n = rand(1,4);
    else if ($type == 'subgenre')
      $n = 1;

    $song_title = '';

    for($i = 0; $i < $n; $i++)
    {
      $song_title .= $faker->word . ' ';
    }

    return $song_title;
  }

  public function make_nick()
  {
    $faker = Faker\Factory::create();

    $n = rand(0,1);

    if ($n == 0)
      return null;
    return $faker->word;
  }
}
