<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // $this->call('MySqlSeeder');
    // $this->call('OracleSeeder');
    $this->call('PostgreSqlSeeder');
    // $this->call('MsSqlSeeder');
  }
}
