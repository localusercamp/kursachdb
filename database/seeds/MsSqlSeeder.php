<?php

use Illuminate\Database\Seeder;

class MsSqlSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $faker = Faker\Factory::create();

    DB::connection('sqlsrv')->table('genre')->insert([
      [
        'title' => 'Rock'
      ],[
        'title' => 'Hip-Hop'
      ],[
        'title' => 'Country'
      ],[
        'title' => 'Jazz'
      ],[
        'title' => 'Electronic'
      ],
    ]);

    for($i = 0; $i < 30; $i++) {
      DB::connection('sqlsrv')->table('subgenre')->insert([
        'title' => $this->make_title('subgenre'),
        'genre_id' => $faker->numberBetween(1,5),
      ]);
    }

    for($i = 0; $i < 40; $i++) {
      DB::connection('sqlsrv')->table('concert')->insert([
        'title' => $faker->state . ' concert',
        'city' => $faker->city,
        'visitors_count' => $faker->randomNumber,
        'premiere_date' => $faker->date,
      ]);
    }

    for($i = 0; $i < 90; $i++) {
      DB::connection('sqlsrv')->table('album')->insert([
        'title' => $this->make_title('album'),
        'created_at' => $faker->date,
      ]);
    }

    for($i = 0; $i < 1000; $i++) {
      DB::connection('sqlsrv')->table('song')->insert([
        'title' => $this->make_title('song'),
        'text' => $faker->text,
        'created_at' => $faker->date,
        'album_id' => $faker->numberBetween(1, 90),
      ]);
    }

    for($i = 0; $i < 30; $i++) {
      DB::connection('sqlsrv')->table('author')->insert([
        'name' => $this->make_title('author'),
        'started_at' => $faker->date,
        'finished_at' => $faker->date,
      ]);
    }

    for($i = 0; $i < 100; $i++) {
      DB::connection('sqlsrv')->table('musician')->insert([
        'firstname' => $faker->firstname,
        'surname' => $faker->lastname,
        'nickname' => $this->make_nick(),
        'author_id' => $faker->numberBetween(1, 30),
      ]);
    }

    for($i = 1; $i < 1001; $i++) {
      DB::connection('sqlsrv')->table('author_song')->insert([
        'author_id' => $faker->numberBetween(1,30),
        'song_id' => $i,
      ]);
    }
  }

  public function make_title($type)
  {
    $faker = Faker\Factory::create();

    $n = 0;

    if ($type == 'album')
      $n = rand(1,2);
    else if ($type == 'author')
      $n = rand(1,2);
    else if ($type == 'song')
      $n = rand(1,4);
    else if ($type == 'subgenre')
      $n = 1;

    $song_title = '';

    for($i = 0; $i < $n; $i++)
    {
      $song_title .= $faker->word . ' ';
    }

    return $song_title;
  }

  public function make_nick()
  {
    $faker = Faker\Factory::create();

    $n = rand(0,1);

    if ($n == 0)
      return null;
    return $faker->word;
  }
}
